# bitbucketsync
## Description
This is a small bash script that allows you to clone a whole bitbucket organization to you laptop.
If the repositories already exeist, it won't do anything otherwize it clones them.

## Requirements
* bash
* jq
* git (with ssh key configured)
* /usr/bin/base64
* sed
* curl
* Non existant file in called /tmp/repositories

## Usage
`./bitbucket_sync`

clone path : where you want it to clone you repositories
clone mode : how you want it to create a tree in the direcotry (if set to tree, it will create folder and sub folder, otherwise it's flat)
